;;;; day11.lisp
;;;; Day 11 solutions

(defpackage aoc-2022.11
  (:use #:cl #:aoc-2022)
  (:export #:part1 #:part2))

(in-package #:aoc-2022.11)

(defclass monkey ()
  ((items :accessor monkey-items :initarg :items :initform nil)
   (op :reader monkey-op :initarg :op)
   (test-divisible-by :reader monkey-test-divisibility-by :initarg :test-divisible-by)
   (test-true :reader monkey-test-true :initarg :test-true)
   (test-false :reader monkey-test-false :initarg :test-false)
   (inspections :accessor monkey-inspections :initform 0)))

(defun parse-op (op)
  (destructuring-bind (operator operand)
      (delimited-string-to-list op :delimiter #\space)
    (let ((literal (when (digit-char-p (char operand 0)) (parse-integer operand))))
      (cond ((string= operator "*")
	     (lambda (val) (* val (or literal val))))
	    ((string= operator "+")
	     (lambda (val) (+ val (or literal val))))))))

(defun read-monkey-from-stream (stream)
  (let ((label (read-line stream nil nil)))
    (when label
      (let ((items (mapcar #'parse-integer
			   (delimited-string-to-list
			    (subseq (read-line stream nil nil) 18))))
	    (op (subseq (read-line stream nil nil) 23))
	    (div (parse-integer (read-line stream nil nil) :start 20))
	    (true (parse-integer (read-line stream nil nil) :start 28))
	    (false (parse-integer (read-line stream nil nil) :start 29)))
	(read-line stream nil nil)
	(make-instance 'monkey
		       :items items
		       :op (parse-op op)
		       :test-divisible-by div
		       :test-true true
		       :test-false false)))))

(defun read-monkeys (source)
  (with-input-from-source (stream source)
    (loop for monkey = (read-monkey-from-stream stream)
	  while monkey
	  collect monkey)))

(defmethod monkey-inspect-item ((monkey monkey) monkeys worry-management)
  (let* ((item (pop (monkey-items monkey)))
	 (new-worry (funcall worry-management (funcall (monkey-op monkey) item)))
	 (test (zerop (mod new-worry (monkey-test-divisibility-by monkey)))))
    (push new-worry (monkey-items (nth (if test
					   (monkey-test-true monkey)
					   (monkey-test-false monkey))
				       monkeys)))
    (incf (monkey-inspections monkey))))

(defun monkey-keep-away (monkeys rounds worry-management)
  (if (zerop rounds)
      monkeys
      (loop for monkey in monkeys
	    do (loop while (monkey-items monkey)
		     do (monkey-inspect-item monkey monkeys worry-management))
	    finally (return (monkey-keep-away monkeys (- rounds 1) worry-management)))))

(defun monkey-business (monkeys)
  (let ((inspections (sort (mapcar #'monkey-inspections monkeys) #'>)))
    (* (first inspections) (second inspections))))

(defun part1 (source)
  (let ((monkeys (read-monkeys source))
	(mgmt (lambda (w) (truncate (/ w 3)))))
    (monkey-business (monkey-keep-away monkeys 20 mgmt))))

(defun part2 (source)
  (let* ((monkeys (read-monkeys source))
	 (worry-divisor (reduce #'* monkeys :key #'monkey-test-divisibility-by))
	 (mgmt (lambda (w) (mod w worry-divisor))))
    (monkey-business (monkey-keep-away monkeys 10000 mgmt))))


