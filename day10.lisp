;;;; day10.lisp
;;;; Day 10 solutions

(defpackage aoc-2022.10
  (:use #:cl #:aoc-2022)
  (:export #:part1 #:part2))

(in-package #:aoc-2022.10)

(defun parse-instruction (line)
  (let ((tokens (delimited-string-to-list line :delimiter #\space)))
    (cond ((string= "noop" (first tokens))
	   (list (lambda (cpu) (declare (ignore cpu)) (values))))
	  ((string= "addx" (first tokens))
	   (list (lambda (cpu) (declare (ignore cpu)) (values))
		 (lambda (cpu) (incf (cpu-x cpu)
				     (parse-integer (second tokens))))))
	  (t (error "Unrecognized instruction ~a" tokens)))))

(defun parse-instructions (source)
  (mapcan #'parse-instruction (read-all-lines source)))

(defclass cpu ()
  ((cycle :accessor cpu-cycle :initform 1)
   (x :accessor cpu-x :initform 1)
   (instructions :accessor cpu-instructions :initarg :instructions)))

(defmethod execute-cycle ((cpu cpu))
  (let ((instruction (pop (cpu-instructions cpu))))
    (when instruction
      (incf (cpu-cycle cpu))
      (funcall instruction cpu))))

(defun signal-strength (cycle x) (* cycle x))

(defun part1 (source)
  (let* ((instructions (parse-instructions source))
	 (cpu (make-instance 'cpu :instructions instructions)))
    (loop repeat 220
	  do (execute-cycle cpu)
	  when (zerop (mod (- (cpu-cycle cpu) 20) 40))
	    collect (signal-strength (cpu-cycle cpu) (cpu-x cpu)) into results
	  finally (return (reduce #'+ results)))))

(defun part2 (source)
  (let* ((instructions (parse-instructions source))
	 (cpu (make-instance 'cpu :instructions instructions)))
    (format t "~%")
    (loop while (cpu-instructions cpu)
	  for pos = (mod (- (cpu-cycle cpu) 1) 40)
	  do (if (<= (- (cpu-x cpu) 1) pos (+ (cpu-x cpu) 1))
		 (princ #\#)
		 (princ #\.))
	     (when (zerop (mod (cpu-cycle cpu) 40))
	       (princ #\newline))
	     (execute-cycle cpu))))


