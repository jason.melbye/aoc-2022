;;;; day9.lisp
;;;; Day 9 solutions

(defpackage aoc-2022.9
  (:use #:cl #:aoc-2022)
  (:export #:part1 #:part2))

(in-package #:aoc-2022.9)

(defparameter *directions*
  '(("U" . (-1 0))
    ("L" . (0 -1))
    ("D" . (1 0))
    ("R" . (0 1))))

(defun make-move (direction distance) (cons direction distance))

(defun move-direction (move) (car move))

(defun move-distance (move) (cdr move))

(defun move-vector (move)
  (cdr (assoc (move-direction move) *directions* :test #'string=)))

(defun parse-line (line)
  (let ((tokens (delimited-string-to-list line :delimiter #\space)))
    (make-move (first tokens)
	       (parse-integer (second tokens)))))

(defun parse-input (source)
  (read-all-lines source :transform #'parse-line))

(defun move (start vector)
  (mapcar #'+ start vector))

(defun sign (number)
  (cond ((< number 0) -1)
	((> number 0) 1)
	(t 0)))

(defun normal-vector (dy dx)
  (list (sign dy) (sign dx)))

(defun tail-vector (head tail)
  (with-point (hy hx) head
    (with-point (ty tx) tail
      (let ((dy (- hy ty))
	    (dx (- hx tx)))
	(if (<= (max (abs dy) (abs dx)) 1)
	    '(0 0)
	    (normal-vector dy dx))))))

(defun part1 (source)
  (let ((moves (parse-input source))
	(visited (make-hash-table :test #'equal)))
    (labels ((do-move (moves head tail)
	       (if (null moves)
		   (hash-table-count visited)
		   (loop repeat (move-distance (first moves))
			 with vec = (move-vector (first moves))
			 for h = (move head vec) then (move h vec)
			 for tvec = (tail-vector h tail) then (tail-vector h tl)
			 for tl = (move tail tvec) then (move tl tvec)
			 do (setf (gethash tl visited) t)
			 finally (return (do-move (rest moves) h tl))))))
      (do-move moves '(0 0) '(0 0)))))

(defun move-rope (rope vec)
  (loop for k from 0 below (length rope)
	for knot = (aref rope k)
	for v = vec then (tail-vector new knot)
	for new = (move knot v)
	do (setf (aref rope k) new)
	finally (return rope)))

(defun part2 (source rope-length)
  (let ((moves (parse-input source))
	(visited (make-hash-table :test #'equal)))
    (labels ((do-move (moves rope)
	       (if (null moves)
		   (hash-table-count visited)
		   (loop repeat (move-distance (first moves))
			 with vec = (move-vector (first moves))
			 for r = (move-rope rope vec) then (move-rope r vec)
			 for tail = (elt r (- rope-length 1))
			 do (setf (gethash tail visited) t)
			 finally (return (do-move (rest moves) r))))))
      (do-move moves (make-array rope-length :initial-element (list 0 0))))))

