;;;; day8.lisp
;;;; Day 8 solutions

(defpackage aoc-2022.8
  (:use #:cl #:aoc-2022)
  (:export #:part1 #:part2))

(in-package #:aoc-2022.8)

(defun parse-line (line)
  (map 'list #'char->integer line))

(defun parse-canopy (source)
  (let* ((data (read-all-lines source :transform #'parse-line))
	 (size (length data)))
    (make-array (list size size) :initial-contents data)))

(defun visible-p (canopy y x)
  (let ((height (aref canopy y x)))
    (not
     (and (loop for i from (- y 1) downto 0
		for h = (aref canopy i x)
		do (when (>= h height) (return t)))
	  (loop for i from (+ y 1) below (array-dimension canopy 0)
		for h = (aref canopy i x)
		do (when (>= h height) (return t)))
	  (loop for i from (- x 1) downto 0
		for h = (aref canopy y i)
		do (when (>= h height) (return t)))
	  (loop for i from (+ x 1) below (array-dimension canopy 1)
		for h = (aref canopy y i)
		do (when (>= h height) (return t)))))))

(defun part1 (source)
  (let ((canopy (parse-canopy source)))
    (loop for y from 0 below (array-dimension canopy 0)
	  sum (loop for x from 0 below (array-dimension canopy 1)
		    count (visible-p canopy y x)))))

(defun viewing-distance (canopy y x direction)
  (destructuring-bind (dy dx) direction
    (let ((height (aref canopy y x)))
      (loop for i = (+ y dy) then (+ i dy)
	    for j = (+ x dx) then (+ j dx)
	    while (valid-array-subscripts-p canopy i j)
	    count 1 into distance
	    when (>= (aref canopy i j) height)
	      do (loop-finish)
	    finally (return distance)))))

(defun scenic-score (viewing-distances)
  (reduce #'* viewing-distances))

(defun part2 (source)
  (let ((canopy (parse-canopy source)))
    (loop for y from 0 below (array-dimension canopy 0)
	  maximize (loop for x from 0 below (array-dimension canopy 1)
			 for distances = (mapcar (lambda (v) (viewing-distance canopy y x v))
						 '((-1 0) (0 -1) (1 0) (0 1)))
			 maximize (scenic-score distances)))))
