;;;; day14.lisp
;;;; Day 14 solutions

(defpackage aoc-2022.14
  (:use #:cl #:aoc-2022)
  (:export #:part1 #:part2))

(in-package #:aoc-2022.14)

(defun parse-line-segments (input)
  (let ((segments (delimited-string-to-list input :delimiter #\-)))
    (loop for segment in segments
	  for pair = (delimited-string-to-list segment)
	  collect (mapcar (lambda (n) (parse-integer (remove #\> n))) pair))))

(defun parse-input (source)
  (read-all-lines source :transform #'parse-line-segments))

(defun sign (number)
  (cond ((< number 0) -1)
	((> number 0) 1)
	(t 0)))

(defun unit-vector (dy dx)
  (list (sign dy) (sign dx)))

(defun map-segments (collection)
  (let ((table (make-hash-table :test #'equal)))
    (loop for segments in collection
	  do (loop for ((x1 y1) (x2 y2)) on segments
		   while y2
		   for dx = (- x2 x1)
		   for dy = (- y2 y1)
		   for vec = (unit-vector dy dx)
		   do (loop for pt = (list y1 x1) then (mapcar #'+ pt vec)
			    do (setf (gethash pt table) t)
			    until (equal pt (list y2 x2))))
	  finally (return table))))

(defun max-y (map)
  (loop for (y x) being the hash-keys in map
	maximize y))

(defun unoccupied-p (y x map max-y)
  (declare (ignore max-y))
  (not (or (gethash (list y x) map))))

(defun drop-sand (y x map max-y unoccupied)
  (cond ((> y max-y) nil)
	((funcall unoccupied (+ y 1) x map max-y)
	 (drop-sand (+ y 1) x map max-y unoccupied))
	((funcall unoccupied (+ y 1) (- x 1) map max-y)
	 (drop-sand (+ y 1) (- x 1) map max-y unoccupied))
	((funcall unoccupied (+ y 1) (+ x 1) map max-y)
	 (drop-sand (+ y 1) (+ x 1) map max-y unoccupied))
	(t (list y x))))

(defun part1 (source y x)
  (let* ((map (map-segments (parse-input source)))
	 (max-y (max-y map)))
    (loop for n from 0
	  for resting-position = (drop-sand y x map max-y #'unoccupied-p)
	  while resting-position
	  do (setf (gethash resting-position map) t)
	  finally (return n))))

(defun unoccupied2-p (y x map max-y)
  (not (or (gethash (list y x) map)
	   (and max-y (= y max-y)))))

(defun part2 (source y x)
  (let* ((map (map-segments (parse-input source)))
	 (max-y (+ (max-y map) 2)))
    (loop for n from 1
	  for resting-position = (drop-sand y x map max-y #'unoccupied2-p)
	  while (not (equal resting-position (list 0 500)))
	  do (setf (gethash resting-position map) t)
	  finally (return n))))
