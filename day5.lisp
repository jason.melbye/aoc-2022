;;;; day5.lisp
;;;; Day 5 solutions

(defpackage aoc-2022.5
  (:use #:cl #:aoc-2022)
  (:export #:part1 #:part2))

(in-package #:aoc-2022.5)

(defun make-move (number from-stack to-stack)
  (list number from-stack to-stack))

(defun move-number (move) (first move))

(defun move-from-stack (move) (second move))

(defun move-to-stack (move) (third move))

(defun update-stacks-with-line (line stacks)
  (loop for stack from 1
	for i from 1 below (length line) by 4
	for item = (char line i)
	do (when (char/= item #\space)
	     (push item (gethash stack stacks)))
	finally (return stacks)))

(defun read-stacks-from-stream (stream)
  (let ((stacks (make-hash-table)))
    (loop for line = (read-line stream nil nil)
	  until (digit-char-p (char line 1))
	  do (update-stacks-with-line line stacks))
    (maphash (lambda (k v) (setf (gethash k stacks) (nreverse v)))
	     stacks)
    stacks))

(defun parse-move (line)
  (let ((tokens (delimited-string-to-list line :delimiter #\space)))
    (make-move (parse-integer (nth 1 tokens))
	       (parse-integer (nth 3 tokens))
	       (parse-integer (nth 5 tokens)))))

(defun read-moves-from-stream (stream)
  (read-line stream nil nil)
  (loop for line = (read-line stream nil nil)
	while line
	collect (parse-move line)))

(defun parse-input-from-source (source)
  (with-input-from-source (stream source)
    (values (read-stacks-from-stream stream)
	    (read-moves-from-stream stream))))

(defun do-move (stacks move)
  (loop repeat (move-number move)
	for item = (pop (gethash (move-from-stack move) stacks))
	do (push item (gethash (move-to-stack move) stacks))
	finally (return stacks)))

(defun do-moves (stacks moves do-move)
  (loop for move in moves do (funcall do-move stacks move)
	finally (return stacks)))

(defun part1 (source)
  (multiple-value-bind (stacks moves)
      (parse-input-from-source source)
    (loop for i from 1 to (hash-table-count (do-moves stacks moves #'do-move))
	  collect (first (gethash i stacks)) into result
	  finally (return (coerce result 'string)))))

(defun do-move2 (stacks move)
  (loop repeat (move-number move)
	for item = (pop (gethash (move-from-stack move) stacks))
	collect item into temp
	finally (loop for item in (nreverse temp)
		      do (push item (gethash (move-to-stack move) stacks))
		      finally (return stacks))))

(defun part2 (source)
  (multiple-value-bind (stacks moves)
      (parse-input-from-source source)
    (loop for i from 1 to (hash-table-count (do-moves stacks moves #'do-move2))
	  collect (first (gethash i stacks)) into result
	  finally (return (coerce result 'string)))))
