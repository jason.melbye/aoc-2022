;;;; aoc-2022.lisp

(defpackage aoc-2022
  (:use #:cl)
  (:export #:aoc-input-filepath
	   #:with-input-from-source
	   #:read-all-lines-from-stream
	   #:read-all-lines
	   #:read-delimited-value
	   #:delimited-stream-to-list
	   #:delimited-string-to-list
	   #:compose
	   #:make-circular
	   #:valid-array-subscripts-p
	   #:wrapped-array
	   #:wrapped-array-base
	   #:wrapped-array-dimensions-mapped
	   #:wrapped-aref
	   #:wrapped-array-base-dimensions
	   #:wrapped-array-base-dimension
	   #:read-wrapped-array
	   #:memoize
	   #:unmemoize
	   #:memoize-clear-cache
	   #:with-memoized-fn
	   #:make-point
	   #:point-dimension
	   #:with-point
	   #:neighbors4
	   #:neighbors4-values
	   #:neighbors8
	   #:neighbors8-values
	   #:char->integer))

(in-package #:aoc-2022)

(defparameter *memoized-fns* (make-hash-table))

(defun aoc-input-filepath (filename)
  (merge-pathnames
   (make-pathname :directory '(:relative "input")
		  :name filename)
   (asdf:system-relative-pathname 'aoc-2022 "")))

(defmacro with-input-from-source ((stream source) &body body)
  `(etypecase ,source
     (pathname (with-open-file (,stream ,source) ,@body))
     (string   (with-input-from-string (,stream ,source) ,@body))
     (stream   (let ((,stream ,source)) ,@body))))

(defun read-all-lines-from-stream (stream &key (transform #'identity))
  (loop for line = (read-line stream nil nil)
     while line
     collect (funcall transform line)))

(defun read-all-lines (source &key (transform #'identity))
  (with-input-from-source (stream source)
    (read-all-lines-from-stream stream :transform transform)))

(defun read-delimited-value (stream &key (delimiter #\,) (transform #'identity) read combine-delimiters-p )
  (when combine-delimiters-p
    (loop for peek = (peek-char nil stream nil nil)
	  while (and peek (char= peek delimiter))
	  do (read-char stream)))
  
  (loop for char = (read-char stream nil nil)
	until (or (null char) (char= char delimiter))
	when char collect char into token
	  finally (return (let ((str (coerce token 'string)))
			    (cond ((and (null char) (null token)) 'eof)
				  ((null token) nil)
				  (read (funcall transform (read-from-string str)))
				  (t (funcall transform str)))))))

(defun delimited-stream-to-list (stream &key (delimiter #\,) (transform #'identity) read combine-delimiters-p)
  (loop for value = (read-delimited-value stream
					  :delimiter delimiter
					  :transform transform
					  :read read
					  :combine-delimiters-p combine-delimiters-p)
     until (eq value 'eof)
     collect value))

(defun delimited-string-to-list (string &key (delimiter #\,) (transform #'identity) read combine-delimiters-p)
  (delimited-stream-to-list
   (make-string-input-stream string)
   :delimiter delimiter
   :transform transform
   :read read
   :combine-delimiters-p combine-delimiters-p))

(defun compose (&rest fns)
  (destructuring-bind (fn1 . rest) (reverse fns)
    (lambda (&rest args)
      (reduce (lambda (v f) (funcall f v))
	      rest
	      :initial-value (apply fn1 args)))))

(defun make-circular (list)
  (setf (cdr (last list)) list))

(defun valid-array-subscripts-p (array &rest subscripts)
  (every (lambda (dim s) (< -1 s dim))
	 (array-dimensions array)
	 subscripts))

(defclass wrapped-array ()
  ((base-array :accessor wrapped-array-base :initarg :base)
   (dimensions-wrapped :accessor wrapped-array-dimensions-wrapped :initarg :dimensions-wrapped)))

(defmethod wrapped-aref ((array wrapped-array) &rest subscripts)
  (with-slots (base-array dimensions-wrapped) array
    (let* ((base-dimensions (array-dimensions base-array))
	   (wrapped-subscripts (mapcar (lambda (subscript base-dimension wrap-p)
					 (if wrap-p
					     (mod subscript base-dimension)
					     subscript))
				       subscripts base-dimensions dimensions-wrapped)))
      (apply #'aref base-array wrapped-subscripts))))

(defmethod wrapped-array-base-dimensions ((array wrapped-array))
  (array-dimensions (wrapped-array-base array)))

(defmethod wrapped-array-base-dimension ((array wrapped-array) axis-number)
  (array-dimension (wrapped-array-base array) axis-number))

(defun read-wrapped-array (source wrapped-dimensions)
  (let* ((lines  (read-all-lines source))
	 (height (length lines))
	 (width  (length (first lines)))
	 (array (make-array (list height width) :initial-contents lines)))
    (make-instance 'wrapped-array
		   :base array
		   :dimensions-wrapped wrapped-dimensions)))

(defun memoize (fn)
  (let ((cache (make-hash-table :test #'equal))
	(original-fn (symbol-function fn)))
    (setf (gethash fn *memoized-fns*) original-fn)
    (setf (symbol-function fn)
	  (lambda (&rest args)
	    (multiple-value-bind (value found-p)
		(gethash args cache)
	      (if found-p
		  value
		  (setf (gethash args cache)
			(apply original-fn args))))))))

(defun unmemoize (fn)
  (let ((original (gethash fn *memoized-fns*)))
    (when original
      (setf (symbol-function fn) original))
    (not (null original))))

(defun memoize-clear-cache (fn)
  (unmemoize fn)
  (memoize fn))

(defmacro with-memoized-fn ((fn) &body body)
  `(progn (memoize ',fn)
	  (unwind-protect (progn ,@body)
	    (unmemoize ',fn))))

(defun make-point (x y &rest more-coords)
  (apply #'list x y more-coords))

(defun point-dimension (point dimension)
  (nth dimension point))

(defmacro with-point (lambda-list point &body body)
  `(destructuring-bind (,@lambda-list &rest rest) ,point
     (declare (ignore rest)) 
     ,@body))

(defun neighbors4 (array y x)
  (remove-if-not (lambda (pt) (apply #'valid-array-subscripts-p array pt))
		 (list (list (- y 1) x)
		       (list y (- x 1))
		       (list y (+ x 1))
		       (list (+ y 1) x))))

(defun neighbors4-values (array y x)
  (mapcar (lambda (index) (apply #'aref array index))
	  (neighbors4 array y x)))

(defun neighbors8 (array y x)
  (remove-if-not (lambda (pt) (apply #'valid-array-subscripts-p array pt))
		 (list (list (- y 1) (- x 1))
		       (list (- y 1) x)
		       (list (- y 1) (+ x 1))
		       (list y (- x 1))
		       (list y (+ x 1))
		       (list (+ y 1) (- x 1))
		       (list (+ y 1) x)
		       (list (+ y 1) (+ x 1)))))

(defun neighbors8-values (array y x)
  (mapcar (lambda (index) (apply #'aref array index))
	  (neighbors8 array y x)))

(defun char->integer (char)
  (ecase char
    (#\0 0)
    (#\1 1)
    (#\2 2)
    (#\3 3)
    (#\4 4)
    (#\5 5)
    (#\6 6)
    (#\7 7)
    (#\8 8)
    (#\9 9)))
