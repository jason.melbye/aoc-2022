;;;; day3.lisp
;;;; Day 3 solutions

(defpackage aoc-2022.3
  (:use #:cl #:aoc-2022)
  (:export #:part1 #:part2))

(in-package #:aoc-2022.3)

(defparameter *priorities*
  (let ((table (make-hash-table)))
    (loop for char across "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	  for code from 1
	  do (setf (gethash char table) code)
	  finally (return table))))

(defun rucksack-compartments (line)
  (let ((midpoint (/ (length line) 2)))
    (list (subseq line 0 midpoint)
	  (subseq line midpoint))))

(defun parse-input (source)
  (read-all-lines source :transform (lambda (line) (coerce line 'list))))

(defun common-item (compartments)
  (first (intersection (first compartments) 
		       (second compartments))))

(defun item-priority (item)
  (gethash item *priorities*))

(defun part1 (source)
  (loop for rucksack in (parse-input source)
	for item = (common-item (rucksack-compartments rucksack))
	sum (item-priority item)))

(defun groups-of (list n)
  (labels ((group (list m current result)
	     (if (null list)
		 (if current (cons current result) result)
		 (let ((item (first list))
		       (rest (rest list)))
		   (if (zerop m)
		       (group rest (- n 1) (list item) (cons current result))
		       (group rest (- m 1) (cons item current) result))))))
    (group list n nil nil)))

(defun group-common-item (group)
  (first (reduce #'intersection group)))

(defun part2 (source)
  (loop for group in (groups-of (parse-input source) 3)
	for item = (group-common-item group)
	sum (item-priority item)))
