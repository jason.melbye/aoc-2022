;;;; day6.lisp
;;;; Day 6 solutions

(defpackage aoc-2022.6
  (:use #:cl #:aoc-2022)
  (:export #:part1 #:part2))

(in-package #:aoc-2022.6)

(defun read-input (source)
  (with-input-from-source (stream source)
    (read-line stream nil nil)))

(defun start-position (buffer marker-length)
  (loop for i from 0 to (- (length buffer) marker-length)
	for data = (subseq buffer i (+ i marker-length))
	for unique = (length (remove-duplicates data))
	until (= unique marker-length)
	finally (return (+ i marker-length))))

(defun part1 (source)
  (start-position (read-input source) 4))

(defun part2 (source)
  (start-position (read-input source) 14))
