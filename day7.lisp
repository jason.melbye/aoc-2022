;;;; day7.lisp
;;;; Day 7 solutions

(defpackage aoc-2022.7
  (:use #:cl #:aoc-2022)
  (:export #:part1 #:part2))

(in-package #:aoc-2022.7)

(defclass terminal ()
  ((stream :initarg :stream)
   (buffer :initform nil)))

(defmethod terminal-read ((term terminal))
  (with-slots (stream buffer) term
      (or (pop buffer)
	  (read-line stream nil nil))))

(defmethod terminal-unread ((term terminal) line)
  (with-slots (buffer) term
    (push line buffer)))

(defun prompt-p (line) (char= (char line 0) #\$))

(defclass dir ()
  ((name :reader dir-name :initarg :name)
   (parent :reader dir-parent :initarg :parent)
   (subdirs :accessor dir-subdirs :initform nil)
   (files :accessor dir-files :initform nil)
   (size :accessor dir-size :initform nil)))

(defun make-file (name size) (list name size))

(defun file-size (file) (second file))

(defun change-dir (term working-dir arg)
  (declare (ignore term))
  (cond ((string= ".." arg)
	 (dir-parent working-dir))
	((member arg (dir-subdirs working-dir) :key #'dir-name :test #'string=)
	 (find arg (dir-subdirs working-dir) :key #'dir-name :test #'string=))
	(t
	 (let ((new (make-instance 'dir :name arg :parent working-dir)))
	   (push new (dir-subdirs working-dir))
	   new))))

(defun list-dir (term working-dir)
  (loop for line = (terminal-read term)
	while (and line (not (prompt-p line)))
	for tokens = (delimited-string-to-list line :delimiter #\space)
	do (unless (string= "dir" (first tokens))
	     (push (make-file (second tokens)
			      (parse-integer (first tokens)))
		   (dir-files working-dir)))
	finally (terminal-unread term line))
  working-dir)

(defparameter *commands*
  (list (cons "cd" #'change-dir)
	(cons "ls" #'list-dir)))

(defun read-command (term)
  (let ((line (terminal-read term)))
    (when line
      (let ((tokens (delimited-string-to-list line :delimiter #\space)))
	(assert (string= "$" (first tokens)))
	(rest tokens)))))

(defun execute-command (term working-dir command args)
  (let ((fn (cdr (assoc command *commands* :test #'string=))))
    (apply fn term working-dir args)))

(defun parse-terminal-output (source)
  (with-input-from-source (stream source)
    (let ((term (make-instance 'terminal :stream stream))
	  (fs (make-instance 'dir :name "filesystem" :parent nil)))
      (loop for wd = fs then (execute-command term wd cmd args)
	    for (cmd . args) = (read-command term)
	    while cmd
	    finally (return fs)))))

(defmethod get-dir-size ((working-dir dir))
  (or (dir-size working-dir)
      (setf (dir-size working-dir)
	    (+ (reduce #'+ (dir-files working-dir) :key #'file-size)
	       (reduce #'+ (dir-subdirs working-dir) :key #'get-dir-size)))))

(defmethod search-dirs ((working-dir dir) test)
  (if (funcall test working-dir)
      (cons working-dir (mapcan (lambda (d) (search-dirs d test)) (dir-subdirs working-dir)))
      (mapcan (lambda (d) (search-dirs d test)) (dir-subdirs working-dir))))

(defun part1 (source)
  (let ((fs (parse-terminal-output source)))
    (get-dir-size fs)
    (reduce #'+
	    (search-dirs fs (lambda (d) (< (dir-size d) 100000)))
	    :key #'dir-size)))

(defun part2 (source)
  (let ((fs (parse-terminal-output source)))
    (get-dir-size fs)
    (let* ((available 70000000)
	   (need 30000000)
	   (used (dir-size fs))
	   (free (- available used))
	   (shortfall (- need free)))
      (dir-size
       (first (sort (search-dirs fs (lambda (d) (>= (dir-size d) shortfall)))
		    #'<
		    :key #'dir-size))))))
