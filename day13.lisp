;;;; day13.lisp
;;;; Day 13 solutions

(defpackage aoc-2022.13
  (:use #:cl #:aoc-2022)
  (:export #:part1 #:part2))

(in-package #:aoc-2022.13)

(defun read-value (stream)
  (let ((peek (peek-char t stream nil nil)))
    (case peek
      ((nil) 'eof)
      (#\] (progn (read-char stream)
		  'end))
      (#\[ (read-list stream))
      (#\, (progn (read-char stream)
		  (read-value stream)))
      (otherwise (read-number stream)))))

(defun end-p (value) (eq value 'end))

(defun read-list (stream)
  (read-char stream nil nil) ; opening [
  (loop for val = (read-value stream)
	until (end-p val)
	collect val))

(defun read-number (stream)
  (loop for peek = (peek-char nil stream nil nil)
	while (and peek (digit-char-p peek))
	collect (read-char stream) into number
	finally (return (and (> (length number) 0)
			     (parse-integer (coerce number 'string))))))

(defun read-packet-pair-from-stream (stream)
  (let ((p1 (read-value stream))
	(p2 (read-value stream)))
    (list p1 p2)))

(defun read-packet-pairs (source)
  (with-input-from-source (stream source)
    (loop while (peek-char t stream nil nil)
	  collect (read-packet-pair-from-stream stream))))

(defun number-compare (a b)
  (cond ((< a b) -1)
	((> a b) 1)
	(t 0)))

(defun list-compare (a b)
  (cond ((and (null a) (null b)) 0)
	((and (numberp a) (numberp b)) (number-compare a b))
	((numberp a) (list-compare (list a) b))
	((numberp b) (list-compare a (list b)))
	((and (null a) b) -1)
	((and a (null b)) 1)
	(t (let ((comp (list-compare (first a) (first b))))
	     (if (zerop comp)
		 (list-compare (rest a) (rest b))
		 comp)))))

(defun packet< (p1 p2) (= -1 (list-compare p1 p2)))

(defun part1 (source)
  (let ((packets (read-packet-pairs source)))
    (loop for (p1 p2) in packets
	  for i from 1
	  when (packet< p1 p2) sum i)))

(defun part2 (source)
  (let* ((packets (apply #'append (read-packet-pairs source)))
	 (d1 '((2)))
	 (d2 '((6)))
	 (sorted (sort (nconc packets (list d1 d2)) #'packet<)))
    (* (+ 1 (position d1 sorted :test #'equal))
       (+ 1 (position d2 sorted :test #'equal)))))
