;;;; day1.lisp
;;;; Day 1 Solutions

(defpackage aoc-2022.1
  (:use #:cl #:aoc-2022)
  (:export #:part1 #:part2))

(in-package #:aoc-2022.1)

(defun not-null-or-empty-p (string)
  (and string (> (length string) 0)))

(defun parse-elf (stream)
  (loop for food = (read-line stream nil nil)
	while (not-null-or-empty-p food)
	collect (parse-integer food)))

(defun parse-elves (source)
  (with-input-from-source (stream source)
    (loop for elf = (parse-elf stream)
	  while elf
	  collect elf)))

(defun elf-calorie-total (elf)
  (reduce #'+ elf))

(defun part1 (source)
  (let* ((elves (parse-elves source))
	 (totals (mapcar #'elf-calorie-total elves)))
    (first (sort totals #'>))))

(defun part2 (source n)
  (let* ((elves (parse-elves source))
	 (totals (sort (mapcar #'elf-calorie-total elves) #'>)))
    (loop for elf in totals
	  repeat n
	  sum elf)))
