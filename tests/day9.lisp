;;;; day9.lisp
;;;; Tests for day 9 solutions

(in-package #:aoc-2022-tests)
(in-suite aoc-2022-tests)

(defparameter *example9-1*
  "R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2")

(defparameter *example9-2*
  "R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20
")

(defparameter *input9* (aoc-2022:aoc-input-filepath "day9"))

(test day9-part1
  (is (= 13 (aoc-2022.9:part1 *example9-1*)))
  (is (= 6243 (aoc-2022.9:part1 *input9*))))

(test day9-part2
  (is (= 13 (aoc-2022.9:part2 *example9-1* 2)))
  (is (= 1 (aoc-2022.9:part2 *example9-1* 10)))
  (is (= 36 (aoc-2022.9:part2 *example9-2* 10)))
  (is (= 2630 (aoc-2022.9:part2 *input9* 10))))
