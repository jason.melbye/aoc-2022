;;;; day12.lisp
;;;; Tests for day 12 solutions

(in-package #:aoc-2022-tests)
(in-suite aoc-2022-tests)

(defparameter *example12-1*
  "Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi")

(defparameter *input12* (aoc-2022:aoc-input-filepath "day12"))

(test day12-part1
  (is (= 31 (aoc-2022.12:part1 *example12-1*)))
  (is (= 520 (aoc-2022.12:part1 *input12*))))

(test day12-part2
  (is (= 29 (aoc-2022.12:part2 *example12-1*)))
  (is (= 508 (aoc-2022.12:part2 *input12*))))
