;;;; day3.lisp
;;;; Tests for day 3 solutions

(in-package #:aoc-2022-tests)
(in-suite aoc-2022-tests)

(defparameter *example3-1*
  "vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw")

(defparameter *input3* (aoc-2022:aoc-input-filepath "day3"))

(test day3-part1
  (is (= 157 (aoc-2022.3:part1 *example3-1*)))
  (is (= 7990 (aoc-2022.3:part1 *input3*))))

(test day3-part2
  (is (= 70 (aoc-2022.3:part2 *example3-1*)))
  (is (= 2602 (aoc-2022.3:part2 *input3*))))
