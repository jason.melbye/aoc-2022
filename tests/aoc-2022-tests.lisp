;;;; aoc-2022-tests.lisp

(defpackage aoc-2022-tests
  (:use #:cl #:fiveam)
  (:export #:aoc-2022-tests
	   #:test-aoc-2022))

(in-package #:aoc-2022-tests)

(def-suite aoc-2022-tests)

(defun test-aoc-2022 ()
  (run! 'aoc-2022-tests))
