;;;; day13.lisp
;;;; Tests for day 13 solutions

(in-package #:aoc-2022-tests)
(in-suite aoc-2022-tests)

(defparameter *example13-1*
  "[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]")

(defparameter *input13* (aoc-2022:aoc-input-filepath "day13"))

(test day13-part1
  (is (= 13 (aoc-2022.13:part1 *example13-1*)))
  (is (= 5605 (aoc-2022.13:part1 *input13*))))

(test day13-part2
  (is (= 140 (aoc-2022.13:part2 *example13-1*)))
  (is (= 24969 (aoc-2022.13:part2 *input13*))))
