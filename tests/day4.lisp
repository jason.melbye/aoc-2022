;;;; day4.lisp
;;;; Tests for day 4 solutions

(in-package #:aoc-2022-tests)
(in-suite aoc-2022-tests)

(defparameter *example4-1*
  "2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8")

(defparameter *input4* (aoc-2022:aoc-input-filepath "day4"))

(test day4-part1
  (is (= 2 (aoc-2022.4:part1 *example4-1*)))
  (is (= 507 (aoc-2022.4:part1 *input4*))))

(test day4-part2
  (is (= 4 (aoc-2022.4:part2 *example4-1*)))
  (is (= 897 (aoc-2022.4:part2 *input4*))))
