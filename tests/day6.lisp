;;;; day6.lisp
;;;; Tests for day 6 solutions

(in-package #:aoc-2022-tests)
(in-suite aoc-2022-tests)

(defparameter *example6-1* "mjqjpqmgbljsphdztnvjfqwrcgsmlb")
(defparameter *example6-2* "bvwbjplbgvbhsrlpgdmjqwftvncz")
(defparameter *example6-3* "nppdvjthqldpwncqszvftbrmjlhg")
(defparameter *example6-4* "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")
(defparameter *example6-5* "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")

(defparameter *input6* (aoc-2022:aoc-input-filepath "day6"))

(test day6-part1
  (is (= 7 (aoc-2022.6:part1 *example6-1*)))
  (is (= 5 (aoc-2022.6:part1 *example6-2*)))
  (is (= 6 (aoc-2022.6:part1 *example6-3*)))
  (is (= 10 (aoc-2022.6:part1 *example6-4*)))
  (is (= 11 (aoc-2022.6:part1 *example6-5*)))
  (is (= 1855 (aoc-2022.6:part1 *input6*))))

(test day6-part2
  (is (= 19 (aoc-2022.6:part2 *example6-1*)))
  (is (= 23 (aoc-2022.6:part2 *example6-2*)))
  (is (= 23 (aoc-2022.6:part2 *example6-3*)))
  (is (= 29 (aoc-2022.6:part2 *example6-4*)))
  (is (= 26 (aoc-2022.6:part2 *example6-5*)))
  (is (= 3256 (aoc-2022.6:part2 *input6*))))
