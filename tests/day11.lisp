;;;; day11.lisp
;;;; Tests for day 11 solutions

(in-package #:aoc-2022-tests)
(in-suite aoc-2022-tests)

(defparameter *example11-1*
  "Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1")

(defparameter *input11* (aoc-2022:aoc-input-filepath "day11"))

(test day11-part1
  (is (= 10605 (aoc-2022.11:part1 *example11-1*)))
  (is (= 61503 (aoc-2022.11:part1 *input11*))))

(test day11-part2
  (is (= 2713310158 (aoc-2022.11:part2 *example11-1*)))
  (is (= 14081365540 (aoc-2022.11:part2 *input11*))))
