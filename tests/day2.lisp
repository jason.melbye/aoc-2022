;;;; day2.lisp
;;;; Tests for day 2 solutions

(in-package #:aoc-2022-tests)
(in-suite aoc-2022-tests)

(defparameter *example2-1*
  "A Y
B X
C Z")

(defparameter *input2* (aoc-2022:aoc-input-filepath "day2"))

(test day2-part1
  (is (= 15 (aoc-2022.2:part1 *example2-1*)))
  (is (= 12276 (aoc-2022.2:part1 *input2*))))

(test day2-part2
  (is (= 12 (aoc-2022.2:part2 *example2-1*)))
  (is (= 9975 (aoc-2022.2:part2 *input2*))))
