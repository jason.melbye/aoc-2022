;;;; day14.lisp
;;;; Tests for day 14 solutions

(in-package #:aoc-2022-tests)
(in-suite aoc-2022-tests)

(defparameter *example14-1*
  "498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9")

(defparameter *input14* (aoc-2022:aoc-input-filepath "day14"))

(test day14-part1
  (is (= 24 (aoc-2022.14:part1 *example14-1* 0 500)))
  (is (= 805 (aoc-2022.14:part1 *input14* 0 500))))

(test day14-part2
  (is (= 93 (aoc-2022.14:part2 *example14-1* 0 500)))
  (is (= 25161 (aoc-2022.14:part2 *input14* 0 500))))
