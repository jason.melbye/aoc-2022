;;;; day5.lisp
;;;; Tests for day 5 solutions

(in-package #:aoc-2022-tests)
(in-suite aoc-2022-tests)

(defparameter *example5-1*
  "    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2")

(defparameter *input5* (aoc-2022:aoc-input-filepath "day5"))

(test day5-part1
  (is (string= "CMZ" (aoc-2022.5:part1 *example5-1*)))
  (is (string= "VJSFHWGFT" (aoc-2022.5:part1 *input5*))))

(test day5-part2
  (is (string= "MCD" (aoc-2022.5:part2 *example5-1*)))
  (is (string= "LCTQFBVZV" (aoc-2022.5:part2 *input5*))))
