;;;; day1.lisp
;;;; Tests for day 1 solutions

(in-package #:aoc-2022-tests)
(in-suite aoc-2022-tests)

(defparameter *example1-1*
  "1000
2000
3000

4000

5000
6000

7000
8000
9000

10000")

(defparameter *input1* (aoc-2022:aoc-input-filepath "day1"))

(test day1-part1
  (is (= 24000 (aoc-2022.1:part1 *example1-1*)))
  (is (= 68467 (aoc-2022.1:part1 *input1*))))

(test day1-part2
  (is (= 45000 (aoc-2022.1:part2 *example1-1* 3)))
  (is (= 203420 (aoc-2022.1:part2 *input1* 3))))
