;;;; day8.lisp
;;;; Tests for day 8 solutions

(in-package #:aoc-2022-tests)
(in-suite aoc-2022-tests)

(defparameter *example8-1*
  "30373
25512
65332
33549
35390")

(defparameter *input8* (aoc-2022:aoc-input-filepath "day8"))

(test day8-part1
  (is (= 21 (aoc-2022.8:part1 *example8-1*)))
  (is (= 1546 (aoc-2022.8:part1 *input8*))))

(test day8-part2
  (is (= 8 (aoc-2022.8:part2 *example8-1*)))
  (is (= 519064 (aoc-2022.8:part2 *input8*))))
