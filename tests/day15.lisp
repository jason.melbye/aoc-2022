;;;; day15.lisp
;;;; Tests for day 15 solutions

(in-package #:aoc-2022-tests)
(in-suite aoc-2022-tests)

(defparameter *example15-1*
  "Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3")

(defparameter *input15* (aoc-2022:aoc-input-filepath "day15"))

(test day15-part1
  (is (= 26 (aoc-2022.15:part1 *example15-1* 10)))
  (is (= 4985193 (aoc-2022.15:part1 *input15* 2000000))))

(test day15-part2
  (is (= 11583882601918 (aoc-2022.15:part2 *input15*))))
