;;;; day7.lisp
;;;; Tests for day 7 solutions

(in-package #:aoc-2022-tests)
(in-suite aoc-2022-tests)

(defparameter *example7-1*
  "$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k")

(defparameter *input7* (aoc-2022:aoc-input-filepath "day7"))

(test day7-part1
  (is (= 95437 (aoc-2022.7:part1 *example7-1*)))
  (is (= 1555642 (aoc-2022.7:part1 *input7*))))

(test day7-part2
  (is (= 24933642 (aoc-2022.7:part2 *example7-1*)))
  (is (= 5974547 (aoc-2022.7:part2 *input7*))))
