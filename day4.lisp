;;;; day4.lisp
;;;; Day 4 solutions

(defpackage aoc-2022.4
  (:use #:cl #:aoc-2022)
  (:export #:part1 #:part2))

(in-package #:aoc-2022.4)

(defun parse-pair (line)
  (mapcar (lambda (elf) (delimited-string-to-list elf :delimiter #\- :transform #'parse-integer)) 
	  (delimited-string-to-list line :delimiter #\,)))

(defun parse-pairs (source)
  (read-all-lines source :transform #'parse-pair))

(defun fully-contained-assignment-p (e1 e2)
  (destructuring-bind (l1 u1) e1
    (destructuring-bind (l2 u2) e2
      (or (<= l1 l2 u2 u1)
	  (<= l2 l1 u1 u2)))))

(defun part1 (source)
  (loop for pair in (parse-pairs source)
	count (apply #'fully-contained-assignment-p pair)))

(defun partially-contained-assignment-p (e1 e2)
  (destructuring-bind (l1 u1) e1
    (destructuring-bind (l2 u2) e2
      (or (<= l1 l2 u1)
	  (<= l2 l1 u2)))))

(defun part2 (source)
  (loop for pair in (parse-pairs source)
	count (apply #'partially-contained-assignment-p pair)))
