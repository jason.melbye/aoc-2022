# aoc-2022



## Advent of Code 2022 Solutions

This project contains my solutions to the [2022 Advent of Code](https://adventofcode.com/2022)

My solutions will generally stick to the following rules I've set for myself:
* Each day's puzzle with have its own package, with functions for part1 and part2.
* Tests will be writen to ensure that solutions are not broken. Each example input and my actual input will be tested.
* I won't include my input files in the project. The code is set up to read input files out of an input test folder, with the day 1 input in a file called ""day1", etc. Participants' input files differ. The solutions for my specific input files are included in the tests.
* When solving part1, I'll try to anticipate things that could be useful for part2 without overly complicating the solutions.
* If my anticipation during part1 was way off the mark, I may re-write part1 when solving part2.
* Code should be readable and reasonably well structured.
* I will stick to the Common Lisp standard library with exceptions for complicated things like crypto/hashing functions.
* I've built up a small set of utilities from prior years that I will make available for this year's puzzles.