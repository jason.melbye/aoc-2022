;;;; aoc-2022.asd

(asdf:defsystem #:aoc-2022
  :description "My Advent of Code 2022 solutions"
  :author "Jason Melbye <jason.melbye@gmail.com>"
  :serial t
  :components ((:file "aoc-2022")
	       (:file "day1")
	       (:file "day2")
	       (:file "day3")
	       (:file "day4")
	       (:file "day5")
	       (:file "day6")
	       (:file "day7")
	       (:file "day8")
	       (:file "day9")
	       (:file "day10")
	       (:file "day11")
	       (:file "day12")
	       (:file "day13")
	       (:file "day14")
	       (:file "day15")))

(asdf:defsystem #:aoc-2022/tests
  :description "Tests to ensure solutions do not break"
  :author "Jason Melbye <jason.melbye@gmail.com"
  :depends-on (:aoc-2022 :fiveam)
  :components ((:module "tests"
		:serial t
		:components ((:file "aoc-2022-tests")
			     (:file "day1")
			     (:file "day2")
			     (:file "day3")
			     (:file "day4")
			     (:file "day5")
			     (:file "day6")
			     (:file "day7")
			     (:file "day8")
			     (:file "day9")
			     (:file "day10")
			     (:file "day11")
			     (:file "day12")
			     (:file "day13")
			     (:file "day14")
			     (:file "day15")))))
