;;;; day2.lisp
;;;; Day 2 Solutions

(defpackage aoc-2022.2
  (:use #:cl #:aoc-2022)
  (:export #:part1 #:part2))

(in-package #:aoc-2022.2)

;; 0 is rock, 1 is paper, 2 is scissors
(defparameter *scores*
  '((0 . ((0 . 3) (1 . 6) (2 . 0)))
    (1 . ((0 . 0) (1 . 3) (2 . 6)))
    (2 . ((0 . 6) (1 . 0) (2 . 3)))))

(defun decode-move (coded-move)
  (ecase coded-move
    ((#\A #\X) 0)
    ((#\B #\Y) 1)
    ((#\C #\Z) 2)))

(defun decode-moves (coded-opponent coded-move)
  (mapcar #'decode-move (list coded-opponent coded-move)))

(defun score-move (move)
  (+ move 1))

(defun score-outcome (opponent move)
  (cdr (assoc move (cdr (assoc opponent *scores*)))))

(defun score-round (opponent move)
  (+ (score-move move)
     (score-outcome opponent move)))

(defun parse-round (stream decode)
  (let ((coded-opponent (read-char stream nil nil)))
    (read-char stream nil nil)
    (let ((coded-move (read-char stream nil nil)))
      (read-line stream nil nil)
      (when coded-move
	(funcall decode coded-opponent coded-move)))))

(defun play-rounds-from-source (source decode)
  (with-input-from-source (stream source)
    (loop for round = (parse-round stream decode)
	  while round
	  sum (apply #'score-round round))))

(defun part1 (source)
  (play-rounds-from-source source #'decode-moves))

(defun decode-result (coded-result)
  (ecase coded-result
    (#\X 0)
    (#\Y 3)
    (#\Z 6)))

(defun decode-moves-part2 (coded-opponent coded-move)
  (let* ((opponent (decode-move coded-opponent))
	 (result (decode-result coded-move))
	 (move (car (rassoc result (cdr (assoc opponent *scores*))))))
    (list opponent move)))

(defun part2 (source)
  (play-rounds-from-source source #'decode-moves-part2))
