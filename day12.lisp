;;;; day12.lisp
;;;; Day 12 solutions

(defpackage aoc-2022.12
  (:use #:cl #:aoc-2022)
  (:export #:part1 #:part2))

(in-package #:aoc-2022.12)

(defparameter *heights*
  (loop for char across "abcdefghijklmnopqrstuvwxyz"
	for height from 0
	collect (cons char height)))

(defun map-height (char)
  (case char
    (#\S (cdr (assoc #\a *heights*)))
    (#\E (cdr (assoc #\z *heights*)))
    (otherwise (cdr (assoc char *heights*)))))

(defun parse-input (source)
  (with-input-from-source (stream source)
    (loop with start = nil
	  with end = nil
	  for row from 0
	  for line = (read-line stream  nil nil)
	  for start-col = (position #\S line)
	  for end-col =  (position #\E line)
	  while line collect (map 'list #'map-height line) into rows
	  when start-col do (setf start (list row start-col))
	  when end-col do (setf end (list row end-col))
	    finally (return (values (make-array (list row (length (first rows))) :initial-contents rows)
				    start
				    end)))))

(defun unvisited-reachable-neighbors (loc map visited)
  (let ((height (apply #'aref map loc)))
    (loop for neighbor in (apply #'neighbors4 map loc)
	  for prev-visit = (apply #'aref visited neighbor)
	  for h = (apply #'aref map neighbor)
	  when (and (not prev-visit) (<= h (+ height 1))) collect neighbor)))

(defun bfs (map start end)
  (let ((visited (make-array (array-dimensions map) :initial-element nil)))
    (labels ((bfs1 (frontier steps)
	       (loop for loc in frontier
		     for prev-visit = (apply #'aref visited loc)
		     do (when (equal loc end) (return-from bfs steps))
			(setf (apply #'aref visited loc) t)
		     when (not prev-visit)
		       nconc (unvisited-reachable-neighbors loc map visited) into new-frontier
		     finally (return (and new-frontier (bfs1 new-frontier (+ steps 1)))))))
      (bfs1 (list start) 0))))

(defun part1 (source)
  (multiple-value-bind (map start end)
      (parse-input source)
    (bfs map start end)))

(defun starting-positions (map)
  (loop for row from 0 below (array-dimension map 0)
	nconc (loop for col from 0 below (array-dimension map 1)
		    for height = (aref map row col)
		    when (zerop height) collect (list row col))))

(defun part2 (source)
  (multiple-value-bind (map start end)
      (parse-input source)
    (declare (ignore start))
    (let* ((starting-positions (starting-positions map))
	   (path-lengths (mapcar (lambda (start) (bfs map start end))
				 starting-positions)))
      (first (sort (remove nil path-lengths) #'<)))))
