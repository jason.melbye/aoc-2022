;;;; day15.lisp
;;;; Day 15 solutions

(defpackage aoc-2022.15
  (:use #:cl #:aoc-2022)
  (:export #:part1 #:part2))

(in-package #:aoc-2022.15)

(defun scanner-data (sensor beacon) (list sensor beacon))

(defun data-sensor (data) (first data))

(defun data-beacon (data) (second data))

(defun manhattan-distance (p1 p2)
  (with-point (x1 y1) p1
    (with-point (x2 y2) p2
      (+ (abs (- x2 x1))
	 (abs (- y2 y1))))))

(defun parse-sensor (text)
  (let ((tokens (delimited-string-to-list text :delimiter #\space)))
    (scanner-data (list (parse-integer (nth 2 tokens) :start 2 :junk-allowed t)
			(parse-integer (nth 3 tokens) :start 2 :junk-allowed t))
		  (list (parse-integer (nth 8 tokens) :start 2 :junk-allowed t)
			(parse-integer (nth 9 tokens) :start 2 :junk-allowed t)))))

(defun parse-data (source)
  (with-input-from-source (stream source)
    (loop for line = (read-line stream nil nil)
	  while line
	  for data = (parse-sensor line)
	  collect data)))

(defun overlapping-range-p (range1 range2)
  (destructuring-bind (l1 u1) range1
    (destructuring-bind (l2 u2) range2
      (or (<= l1 l2 u1)
	  (<= l2 l1 u2)))))

(defun merge-ranges (range1 range2)
  (destructuring-bind (l1 u1) range1
    (destructuring-bind (l2 u2) range2
      (list (min l1 l2)
	    (max u1 u2)))))

(defun range-length (range)
  (+ (- (second range) (first range)) 1))

(defun consolidate-ranges (ranges)
  (labels ((consolidate (wip ranges result)
	     (let ((first (first ranges))
		   (rest (rest ranges)))
	       (cond ((null first) (cons wip result))
		     ((overlapping-range-p first wip)
		      (consolidate (merge-ranges first wip) rest result))
		     (t
		      (consolidate first rest (cons wip result)))))))
    (let ((sorted (sort ranges #'< :key #'first)))
      (consolidate (first sorted) (rest sorted) nil))))

(defun scan-row (sensors row)
  (loop for data in sensors
	for dist = (manhattan-distance (data-sensor data) (data-beacon data))
	for (x y) = (data-sensor data)
	for excess = (- dist (abs (- y row)))
	when (> excess 0) collect (list (- x excess) (+ x excess)) into ranges
	  finally (return (consolidate-ranges ranges))))

(defun part1 (source row)
  (let* ((data (parse-data source))
	 (scan (scan-row data row))
	 (coverage (reduce #'+ scan :key #'range-length))
	 (beacons (remove-duplicates (mapcar #'data-beacon data) :test #'equal))
	 (row-beacons (count row beacons :key #'second)))
    (- coverage row-beacons)))

(defun tuning-frequency (x y) (+ (* x 4000000) y))

(defun part2 (source)
  (let ((data (parse-data source)))
    (loop for row from 0 to 4000000
	  for scan = (scan-row data row)
	  while (= 1 (length scan))
	  finally
	     (return (tuning-frequency (1+ (cadar (sort scan #'< :key #'first)))
				       row)))))
